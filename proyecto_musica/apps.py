from django.apps import AppConfig


class ProyectoMusicaConfig(AppConfig):
    name = 'proyecto_musica'
